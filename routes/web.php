<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/admin', 'IndexController@admin');
Route::get('/', 'IndexController@index');
Route::get('/new-article', 'IndexController@create');
Route::post('/new-article', 'IndexController@store');
Route::get('/edit','IndexController@haledit');
Auth::routes();


Route::get('/{slash}', [
    'uses' => 'IndexController@singlepost',
    'as' => 'site.post'
]);

Route::get('/{id}/edit','IndexController@edit');
Route::put('/{id}','IndexController@update');
Route::delete('/{id}', 'IndexController@destroy');
Route::post('/{articles}/comment', 'CommentController@store')->name('articles.comment.store');
Route::post('/article/like','LikeController@articlelike')->name('articlelike');
// Route::get('/{id}/editComment', 'CommentController@edit');
// Route::put('/{id}', 'CommentController@update');
// Route::delete('/{id}', 'CommentController@destroy');
//Route::post('/like', 'IndexController@likeArticle')->name('like');
