@extends('layouts.master')

@section('content')
	<div class="container">
		<div class="row">
    		<div class="col-sm-12">
    			<div class="card-body">
					<table id="article" class="table table-bordered table-striped">
					<thead>
					  <tr class="text-center">
					    <th>No</th>
					    <th>Judul</th>
					    <th>User ID</th>					    
					    <th>Create</th>
					    <th>Update</th>
					    <th>Action</th>			    
					  </tr>
					 </thead>


					 <tbody>
				  		@foreach($article as $key => $article)
						
					  	<tr style="text-align: center;">	  		
						    <td> {{ $key + 1}} </td>
						    <td> {{ $article->title}} </td>							    			    
						    <td> {{ $article->user_id}} </td>				    
						    <td> {{ $article->created_at}} </td>
						    <td> {{ $article->updated_at}} </td>
						    <td>
						    	<a href="{{route('site.post', $article->slash)}}">
                 					<button class="btn btn-primary btn-sm"><i class="fas fa-eye text-white"></i></button>
                 				</a>      
						    	<a href="/{{$article->id}}/edit">
                 					<button class="btn btn-success btn-sm"><i class="fas fa-edit text-white"></i></button>
                 				</a>                 			
                 			
                 				<form action="/{{$article->id}}" method="post" style="display:inline">
				                    @csrf
				                    @method('DELETE')
				                    <!-- <input type="submit" value="delete"> -->
				                    <button class="btn btn-danger btn-sm"><i class="fas fa-trash-alt text-white"></i>
				                    </button>
                				</form>
                			</td>
						 </tr>			
				  		@endforeach 
				  		
				  	</tbody>
				  
					</table>
				</div>
    		</div>
    	</div>
    </div>

@endsection