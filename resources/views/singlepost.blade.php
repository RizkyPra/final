@extends('layouts.master')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-12">
        <div class="post-title" style="text-align: center">
            {{$article->title}} <br>
        </div>
        <div class="post-body mt-4" style="text-align: center">
        	Dibuat : {{$article->created_at}} <br> Diperbarui : {{$article->updated_at}} <br><br>
        </div>
        <div class="post-body mt-4">
            {!!$article->body!!}  
        </div>
        
        <div class="button-footer">
            <span class="btn btn-default btn-xs {{$article->YouLiked()?"liked":""}}" onclick="articlelike('{{$article->id}}',this)"><i class="fa fa-thumbs-up"></i>Like</span>
            <span class="btn btn-default btn-xs">2</span>

        </div>


        <div class="card">
          <div class="card-header">
            Tambahkan Komentar
          </div>
                  <div class="card-body">
                    <blockquote class="blockquote mb-0">
                      
                    <div class="post-body mt-4">
                        @foreach ($article->comments()->get() as $comment)
                        <h3 style="display: inline;">{{$comment->user->name}}</h3> - <h6 style="display: inline;">{{$comment->created_at->diffForHumans()}}</h6>
                        @if ($comment->user_id == Auth::id())
                            <a href="/{{$comment->id}}/editComment">
                                <button class="btn btn-success btn-sm"><i class="fas fa-edit text-white"></i></button>
                            </a>
                        <form action="/{{$comment->id}}" method="post" style="display: inline;">
                            @csrf
                            @method('DELETE')
                            <!-- <input type="submit" value="delete"> -->
                            <button class="btn btn-danger btn-sm"><i class="fas fa-trash-alt text-white"></i></button>                    
                        </form>

                        @endif
                        <p>{{$comment->comment}}</p>

                    @endforeach

                    <form action="{{ route('articles.comment.store', $article)}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <textarea name="comment" id="" cols="30" rows="5" class="form-control" placeholder="Berikan Komentar...."></textarea>
                        </div>
                        <div class="form-group">
                            <input type="submit" name="" class="btn btn-primary" value="Komentar">
                        </div>
                    </form>
                </div>
              
            </blockquote>
          </div>
        </div>        
    </div>
</div>
</div>

<script type="text/javascript">
    function articlelike(articleId, elem){
        var csrfToken = '{{csrf_token()}}';
        $.article('{{route('articlelike')}}', {articleId:articleId,_token:csrfToken}, function(data){
            console.log(data);
        });
    }

</script>


@endsection

