@extends('layouts.master')

@section('content')
<div class="container">
<div class="row">
    <div class="col-sm-12">
        
        <div class="post-body mt-4">
            Tambahkan Komentar  
        </div>
        <div class="post-body mt-4">           
            <form action="/{{$comment->id}}" method="POST">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <textarea name="comment" id="" cols="30" rows="5" class="form-control" placeholder="Berikan Komentar....">{{$comment->comment}}</textarea>
                </div>
                <div class="form-group">
                    <input type="submit" name="" class="btn btn-primary" value="Update Komentar">
                </div>
            </form>
        </div>
    </div>
</div>
</div>

@endsection