@extends('layouts.master')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-sm-12">
            <form action="" method="POST">
                @csrf
                <form>
                    <div class="form-group">
                      <label for="exampleFormControlInput1">Title</label>
                      <input type="text" name="title" class="form-control" id="exampleFormControlInput1">
                    </div>
                    
                    <div class="form-group">
                      <label for="exampleFormControlTextarea1">Body</label>
                      <textarea class="form-control" name="body"></textarea>
                    </div>
                    <input type="submit" class="btn btn-primary">
                  </form>

            </form>
        </div>
    </div>
</div>

@push('scripts')
<!-- panggil jquery -->

<!-- panggil ckeditor.js -->
<script type="text/javascript" src="{{asset('ckeditor/ckeditor.js')}}"></script>
<!-- panggil adapter jquery ckeditor -->

<!-- setup selector -->
<script type="text/javascript">
    CKEDITOR.replace( 'body' );
</script>
@endpush

@endsection