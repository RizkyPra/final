<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    protected $fillable = ['title', 'body', 'user_id'];
    //
    use LikeTrait;

	public function user()
	{
	    return $this->belongsTo('App\User');
	}

	public function likes()
	{
	    return $this->hasMany('App\Like');
	}

	public function comments()
	{
		return $this->hasMany(Comment::class);
	}

}
