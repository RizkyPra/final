<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\Like;
use Illuminate\Support\Facades\Input;

class LikeController extends Controller
{
    public function articlelike()
    {
    	$articleId=Input::get('articleId');
    	$article=Article::find($articleId);


    	if(!$article->YouLiked()){
    		$article->YoulikeIt();
    		return response()->json(['status'=>'success','message'=>'liked']);
    	}else{
    		$article->YouUnlike();
    		return response()->json(['status'=>'success','message'=>'unliked']);
    	}

    }
}
