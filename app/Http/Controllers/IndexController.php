<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\User;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;

class IndexController extends Controller
{
    public function admin()
    {
        $data = Article::all();        
        return view('admin', ['article' => $data]);        
    }

    public function index()
    {
        $data = Article::all();

        return view('index', ['data' => $data]);
    }

    public function create()
    {
        return view('createpost');
    }


    public function store(Request $request)
    {
        $articles = new Article();
        $articles->title = $request->title;
        $articles->body = $request->body;
        $articles->slash = Str::slug($request->title , '-');
        $articles->user_id = Auth::id();
        $articles->save();
        return 
        redirect('/');
        
    }

    public function singlepost($slash)
    {
        $article = Article::where('slash', '=', $slash)->first();
        return view ('singlepost', ['article' => $article]);
    }

    public function haledit()
    {
        $articles = Article::where('user_id', '=', Auth::id())->get();
        // dd($articles);        
        return view('indexEdit', ['article' => $articles]);
    }

    public function edit($id)
    {
        $article = Article::where('id', '=', $id)->first();
        // dd($article->body);
        return view ('edit', ['article' => $article]);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $articles = Article::where('id', '=', $id)->first();
        $articles->title = $request->title;
        $articles->body = $request->body;
        $articles->slash = Str::slug($request->title , '-');
        $articles->user_id = Auth::id();
        $articles->save();
        return redirect('/');
    }

    public function destroy($id)
    {
        Article::destroy($id);
        return redirect('/');
    }

    
}
