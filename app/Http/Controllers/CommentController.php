<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Article;
use App\User;
use App\Comment;
use Illuminate\Support\Facades\Auth;

class CommentController extends Controller
{
    // 
    public function store(Request $request, Article $articles)
    {
    	// dd($articles);
    	//dd(request($article));
    	// Comment::create([
    	// 		'article_id' => $articles->id,
    	// 		'user_id' => auth()->id(),
    	// 		'comment' => $request->comment,
    	// ]);
    	$articles->comments()->create(array_merge(
    									$request->only('comment'),
    									['user_id' => auth()->id()]
    								));

    	return redirect()->back();
    }

    public function edit($id)
    {
        $comment = Comment::where('id', '=', $id)->first();
        // dd($comment->comment);        
        return view ('editComment', ['comment' => $comment]);
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $comments = Comment::where('id', '=', $id)->first();
        $comments->comment = $request->comment;       
        $comments->save();
        return redirect('/');
    }
    
    public function destroy($id)
    {
        Comment::destroy($id);
        return redirect()->back();
    }
}
